

"use strict"



//1

//створюємо всі елементи
const board = document.querySelector('.board');
board.style.display = 'grid';
board.style.gridTemplateColumns = 'repeat(5, 1fr)';
board.style.textAlign = 'center';
board.style.listStyleType = 'none';

//Створюємо массив з усіма можливими кнопками
const keyBtns = document.querySelectorAll('button');

//Додаємо кожній кнопці значення кольору в вигляді нового атрибуту
keyBtns.forEach(element => {
  element.baseColor = 0;
})

//Створюємо массив з необхідними кольорами кнопок (діставати ці кольори будемо за допомогою атрибуту кольору що створили перед)
const colors = ['blue', 'black',]

//Створюємо змінну базового кольору для зручного перебирання кольорів
// let baseColor = 0;

//Створюємо івент на натискання клавіш
document.addEventListener('keydown', function(event) {
  
  //В межах івенту перебираємо массив з кнопками
  keyBtns.forEach(element => {

      //Шукаємо кнопку текст якої співпадає з натиснутою клавішею і задаємо ій значення базового кольору
      if(element.innerText == event.key){
      
      //Задаємо натиснутій клавіші базовий колір
      element.style.backgroundColor = colors[element.baseColor];

      //підвищуємо значення кольору
      element.baseColor++;

      }
      //При повторному натисканні на клавішу базовий колір буде змінюватись
    });
       
})